<?php
function auth_page($atts, $content = null){
	extract(shortcode_atts(array(
		'roles' => '1',
	), $atts ) );
		
	$roles = explode(',',$atts['roles']);

	if(is_user_logged_in()){
		
		$user = new WP_User(get_current_user_id());
		$user_role = ($user->roles);

		if(is_array($roles)){
			
			for($i = 0; ; $i++){

				if(!empty($user_role[$i])){

					foreach($roles as $role){

						if($user_role[$i] == $role ){

							return $content;

						}
					
					}

				} else {

					break;
					
				}

			}

		}

	} elseif(in_array('none', $roles)){

		return $content;

	}

}

add_shortcode('page_auth', 'auth_page');
?>