Add as following the Wordpress shortcode to you page:   

 
    :::php
    [page_auth roles='']
 
After calling this shortcode, add the content you want for the specific role. 
After that, you can close the shortcode:

    :::php
    [/page_auth]



The roles can be any role that exists in the Wordpress standard:
- Administrator
- Editor
- Author
- Contributor
- Subscriber

Beside, you can add a custom role 'none', which means that content you asign to this role, is available for non-users.


You can add this roles to the shortcode. For example:
 
    :::php
    [page_auth roles='author,editor,administrator']
 


Try your own!